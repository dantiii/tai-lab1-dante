#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <list>
#include <map>
#include <cmath>

/**
 * Na teoria da informação, a entropia é uma medida da incerteza em uma variável aleatória.
 *
 * Links:
 * https://planetcalc.com/2476/#
 * https://www.shannonentropy.netmark.pl/
 * https://youtu.be/IaTzi_bsuJU?t=729 (map<cahr, int>)
 */

using namespace std;
void fcm();

// int main(int argc, char** argv) {}
int main() {
    cout << "Information models for prediction (Entropy)" << endl;
    fcm();
    return 0;
}

bool existLetter(list<char> &vetor, char letter) {
    for(char const l: vetor) {
        if(letter == l)
            return true;
    }
    return false;
}

void fcm() {
    FILE *file;
    char letter;
    list<char> alphabet;
    map<char, int> symbolQuant;
    int letterCounter = 0;

    file = fopen("text.txt", "r");

    if(file == NULL) {
        cout << "Não foi possível abrir o arquivo." << endl;
        getchar();
        exit(0);
    }

    while(letter != EOF) {
        letter = fgetc(file);
        if(letter == EOF) {
            break;
        }
        letterCounter++;
//        if (letter == ' ') {
//            continue;
//        }
        // cout << "Character: " << letter << endl;
        if(!existLetter(alphabet, letter)) {
            // add on list
            alphabet.push_back(letter);

            // add on map initing with 1 occurrence
            symbolQuant.insert(pair<char, int>(letter, 1));
        } else {
            // increment + 1 occurance of this letter
            symbolQuant[letter]++;
        }
    }
    fclose(file);

    // Show list
    cout << "\n~ ~ ~ Showing symbol list ~ ~ ~\n";
    for(char const l: alphabet) {
        cout << "Symbol :: '" << l << "'" << endl;
    }
    cout << "# symbols: " << size(alphabet) << endl;
    cout << "# Text length: " << letterCounter << endl;
    // --- --- ---
    cout << "\n~ ~ ~ Showing symbols occurence map list ~ ~ ~\n";
    map<char, int>::iterator it;
    for (it = symbolQuant.begin(); it != symbolQuant.end(); ++it) {
        cout << "Symbol '" << it->first << "', # " << it->second << endl;
    }

    // Calculating entropy of information
    float entropy = 0.0;
    map<char, int>::iterator iter;
    for (iter = symbolQuant.begin(); iter != symbolQuant.end(); ++iter) {
        float pxi = (float) (iter->second) / letterCounter;
        cout << "P(" << iter->first << ") :: " << pxi << endl;
        entropy += (pxi) * ((log2 (1 / pxi)));
    }
    cout << "Entropy :: " << entropy << endl;
}